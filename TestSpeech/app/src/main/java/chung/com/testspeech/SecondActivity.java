package chung.com.testspeech;

import android.os.Bundle;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by chung on 5/19/16.
 */
public class SecondActivity extends AppCompatActivity {
    private FragmentTest fragmentTest;
    private Fragment_Search fragmentSearch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        initFragment();
    }

    private void initFragment() {
        fragmentTest = new FragmentTest();
        fragmentSearch = new Fragment_Search();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.framelayout, fragmentTest);
        transaction.add(R.id.framelayout, fragmentSearch);
        transaction.hide(fragmentSearch);
        transaction.commit();
    }

    public void showFragmentSearch() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.show(fragmentSearch);
        transaction.hide(fragmentTest);
        transaction.commit();
    }
}
