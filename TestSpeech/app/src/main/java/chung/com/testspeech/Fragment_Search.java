package chung.com.testspeech;

import android.app.Fragment;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by chung on 5/19/16.
 */
public class Fragment_Search extends Fragment implements View.OnClickListener {
    public static final int REQUEST_CODE = 100;
    private EditText edtSearch;
    private ImageView ivVoice;
    private WebView mWebView;
    private String query;
    public static final String LINK = "https://www.google.com/search?q=";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        edtSearch = (EditText) view.findViewById(R.id.edtSearch);
        ivVoice = (ImageView) view.findViewById(R.id.ivVoice);
        ivVoice.setOnClickListener(this);
        setSpeechInput();
        mWebView = (WebView) view.findViewById(R.id.mWedView);
        mWebView.setWebViewClient(new HelloWebViewClient());
        mWebView.getSettings().setLoadsImagesAutomatically(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

    }


    private void setSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Say something");
        try {
            startActivityForResult(intent, REQUEST_CODE);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getActivity(), a.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == getActivity().RESULT_OK && data != null) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String txtSearch = result.get(0);
            edtSearch.setText(txtSearch);
            if (txtSearch != null) {
                mWebView.loadUrl(LINK + edtSearch.getText().toString());
            }
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ivVoice) {
            setSpeechInput();
        }
    }

    public void searchInWed(String txtSearch) {
        Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
        intent.putExtra(SearchManager.QUERY, txtSearch);
        startActivity(intent);
    }

    class HelloWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

}
