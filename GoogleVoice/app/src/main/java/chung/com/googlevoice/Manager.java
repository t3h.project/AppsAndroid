package chung.com.googlevoice;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by chung on 5/23/16.
 */
public class Manager {
    public static final String[] COLUMNS = {"_id", "_data", "title", "_size", "duration", "artist", "album"};
    private Context context;
    private ContentResolver resolver;
    private ArrayList<Contact> list;
    private ArrayList<Song> listSong;

    public Manager(Context context) {
        this.context = context;
        resolver = context.getContentResolver();

    }

    public ArrayList<Contact> getAllContact() {
        list = new ArrayList<>();
        Cursor cursor = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            //    Toast.makeText(context, name + ":" + phoneNumber, Toast.LENGTH_LONG).show();
            list.add(new Contact(name, phoneNumber));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public ArrayList<Song> getAllSong() {
        listSong = new ArrayList<>();
        Cursor cursor = resolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, COLUMNS, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int id = cursor.getInt(0);
            String data = cursor.getString(1);
            String title = cursor.getString(2);
            float size = cursor.getFloat(3);
            float duration = cursor.getFloat(4);
            String artist = cursor.getString(5);
            String album = cursor.getString(6);
            listSong.add(new Song(id, data, title, artist, album, duration, size));
            cursor.moveToNext();
        }
        return listSong;
    }

}
