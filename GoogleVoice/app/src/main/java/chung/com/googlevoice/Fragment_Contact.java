package chung.com.googlevoice;

import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by chung on 5/26/16.
 */
public class Fragment_Contact extends Fragment implements View.OnClickListener {
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerViewAdapter adapter;
    private EditText edtSearch_Contact;
    private ImageView ivVoice_Contact;
    private Manager manager;
    private ArrayList<Contact> list;
    private TextView tvContactName1, tvContactPhone1;
    private LinearLayout mLinearSearch;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        manager = new Manager(getActivity());
        list = new ArrayList<>();
        list.addAll(manager.getAllContact());
        edtSearch_Contact = (EditText) view.findViewById(R.id.edtSearch_Contact);
        ivVoice_Contact = (ImageView) view.findViewById(R.id.ivVoice_Contact);
        ivVoice_Contact.setOnClickListener(this);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.mRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerViewAdapter(getActivity(), list);
        mRecyclerView.setAdapter(adapter);
        tvContactName1 = (TextView) view.findViewById(R.id.tvContactName1);
        tvContactPhone1 = (TextView) view.findViewById(R.id.tvContactPhone1);
        mLinearSearch = (LinearLayout) view.findViewById(R.id.mLinearSearch);
        mLinearSearch.setVisibility(View.GONE);
//        setSpeechInput();
    }
//goi dien de sang fragment contact ben fragment nay neu goi ten khong==search goi+ten==goi, xong ong thoat han app de vao fragment dau roi goi ten bai hat bat ky trong may test cai bat nhac xem duoc khong
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivVoice_Contact:
                setSpeechInput();
                String textTest = edtSearch_Contact.getText().toString();
                for (Contact contact : list
                        ) {
                    if (textTest.toLowerCase().contains("gọi") && textTest.toLowerCase().contains(contact.getName().toLowerCase())) {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + contact.getPhoneNo()));
                        startActivity(intent);
                    }
                    if (textTest.toLowerCase().contains(contact.getName().toLowerCase())) {
                        mLinearSearch.setVisibility(View.VISIBLE);
                        tvContactName1.setText(contact.getName());
                        tvContactPhone1.setText(contact.getPhoneNo());
                    }
                }
                break;
        }
    }

    private void setSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Đang nghe");
        try {
            startActivityForResult(intent, Fragment_Search.REQUEST_CODE);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getActivity(), a.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Fragment_Search.REQUEST_CODE && resultCode == getActivity().RESULT_OK && data != null) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String txtSearch = result.get(0);
            //goi
            for (Contact contact : list
                    ) {
                if (txtSearch.toLowerCase().contains("gọi") && txtSearch.toLowerCase().contains(contact.getName().toLowerCase())) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + contact.getPhoneNo()));
                    startActivity(intent);
                }
                //search
                if (txtSearch.toLowerCase().contains(contact.getName().toLowerCase())) {
                    mLinearSearch.setVisibility(View.VISIBLE);
                    tvContactName1.setText(contact.getName());
                    tvContactPhone1.setText(contact.getPhoneNo());
                }
            }

        }

    }
}
