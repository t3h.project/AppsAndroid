package chung.com.googlevoice;

/**
 * Created by chung on 5/23/16.
 */
public class Contact {
    private String name;
    private String PhoneNo;

    public Contact(String name, String phoneNo) {
        this.name = name;
        PhoneNo = phoneNo;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }
}
