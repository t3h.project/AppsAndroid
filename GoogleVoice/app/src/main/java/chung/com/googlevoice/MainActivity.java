package chung.com.googlevoice;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private FragmentStart fragmentStart;
    private Fragment_Search fragmentSearch;
    private Fragment_Contact fragmentContact;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initFragment();
    }

    private void initFragment() {
        fragmentStart = new FragmentStart();
        fragmentSearch = new Fragment_Search();
        fragmentContact = new Fragment_Contact();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.framelayout, fragmentStart);
        transaction.add(R.id.framelayout, fragmentSearch);
        transaction.add(R.id.framelayout,fragmentContact);
        transaction.hide(fragmentSearch);
        transaction.hide(fragmentContact);
        transaction.commit();
    }

    public void showFragmentSearch() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.show(fragmentSearch);
        transaction.hide(fragmentStart);
        transaction.hide(fragmentContact);
        transaction.commit();
    }
    public void showFragmentContact() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.show(fragmentContact);
        transaction.hide(fragmentStart);
        transaction.hide(fragmentSearch);
        transaction.commit();
    }
}
