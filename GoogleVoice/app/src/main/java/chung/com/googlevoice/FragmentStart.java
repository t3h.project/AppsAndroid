package chung.com.googlevoice;

import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by chung on 5/19/16.
 */
public class FragmentStart extends Fragment implements View.OnClickListener {
    public static final int REQUEST_CODE = 100;
    private static final String TAG = "MainActivity";
    private ImageView imVoice;
    private TextView tvResult;
    private List<ApplicationInfo> applist;
    private PackageManager packageManager;
    private WifiManager wifiManager;
    private BluetoothAdapter bluetoothAdapter;
    private MainActivity mainActivity;
    private ArrayList<Song> listSong;
    private Manager manager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment, null);
        mainActivity = (MainActivity) getActivity();
        manager = new Manager(mainActivity);
        new LoadApplications().execute();
        initView(view);
        return view;
    }

    private void initView(View view) {
        packageManager = getActivity().getPackageManager();
        listSong = new ArrayList<>();
        listSong.addAll(manager.getAllSong());
        imVoice = (ImageView) view.findViewById(R.id.imVoice);
        tvResult = (TextView) view.findViewById(R.id.tvResult);
        imVoice.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.imVoice) {
            setSpeechInput();

        }
    }

    private void setSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Đang nghe");
        try {
            startActivityForResult(intent, REQUEST_CODE);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getActivity(), a.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == getActivity().RESULT_OK && data != null) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String txtResult = result.get(0);
            tvResult.setText(txtResult);
            for (ApplicationInfo info : applist) {
                String appName = (String) info.loadLabel(packageManager);
                if (txtResult.toLowerCase().contains(appName.toLowerCase())) {
                    Intent intentRun = packageManager
                            .getLaunchIntentForPackage(info.packageName);
                    startActivity(intentRun);
                }
            }
            switch (txtResult) {
                case "ok google":
                    setSpeechInput();
                    break;
                case "khoá máy":

                    break;
                case "mở máy":
                    break;
            }
            if (txtResult.toLowerCase().contains("tìm kiếm")) {
                mainActivity.showFragmentSearch();
            }
            if (txtResult.toLowerCase().contains("bật wifi")) {
                setWifi(true);
            }
            if (txtResult.toLowerCase().contains("tắt wifi")) {
                setWifi(false);
            }
            if (txtResult.toLowerCase().contains("bật bluetooth")) {
                setBluetooth(true);
            }
            if (txtResult.toLowerCase().contains("tắt bluetooth")) {
                setBluetooth(false);
            }
            if (txtResult.toLowerCase().contains("bật dữ liệu")) {
                setDataMobile(true);
            }
            if (txtResult.toLowerCase().contains("tắt dữ liệu")) {
                setDataMobile(false);
            }
            if (txtResult.toLowerCase().contains("")) {
                mainActivity.showFragmentContact();
            }
            //quen mat chua chinhr ben nayf ong thuwr build di tesst goij ddien de sang fragment 2
            // test doan nhac chua the nhe t di ngu dat.ok :)
            for (Song song : listSong) {
                if (txtResult.toLowerCase().contains(song.getTitle().toLowerCase())) {
                    Intent intent = new Intent();
                    intent.setAction(android.content.Intent.ACTION_VIEW);
                    File file = new File(song.getData());
                    intent.setDataAndType(Uri.fromFile(file), "audio/*");
                    startActivity(intent);
                }
            }

        }

    }

    public void setWifi(Boolean enable) {
        wifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled() && enable) {
            wifiManager.setWifiEnabled(true);
        }
        if (wifiManager.isWifiEnabled() && !enable) {
            wifiManager.setWifiEnabled(false);
        }
    }

    public void setBluetooth(Boolean enable) {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter.isEnabled() && !enable) {
            bluetoothAdapter.disable();
        }
        if (!bluetoothAdapter.isEnabled() && enable) {
            bluetoothAdapter.enable();
        }
    }

    public void setDataMobile(boolean enable) {
        ConnectivityManager conman = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class conmanClass = Class.forName(conman.getClass().getName());
            final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
            iConnectivityManagerField.setAccessible(true);
            final Object iConnectivityManager = iConnectivityManagerField.get(conman);
            final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
            final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            setMobileDataEnabledMethod.setAccessible(true);
            setMobileDataEnabledMethod.invoke(iConnectivityManager, enable);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
//de mai t muon may thg e roi test chu khong test duoc bang may that tu` qua.  OKco khi la do android 6.0 len bi nhu the

    private List<ApplicationInfo> checkForLaunchIntent(List<ApplicationInfo> list) {
        ArrayList<ApplicationInfo> applist = new ArrayList<ApplicationInfo>();
        for (ApplicationInfo info : list) {
            try {
                if (null != packageManager.getLaunchIntentForPackage(info.packageName)) {
                    applist.add(info);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return applist;
    }

    public class LoadApplications extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            applist = checkForLaunchIntent(packageManager.getInstalledApplications(PackageManager.GET_META_DATA));
            return null;
        }

    }

}
