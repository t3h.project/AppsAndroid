package chung.com.googlevoice;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by chung on 5/26/16.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

    private ArrayList<Contact> list = new ArrayList<>();
    private Context context;

    public RecyclerViewAdapter(Context context, ArrayList<Contact> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_contact, viewGroup, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.tvContactName.setText(list.get(position).getName());
        holder.tvContactPhone.setText(list.get(position).getPhoneNo());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvContactName, tvContactPhone;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvContactName = (TextView) itemView.findViewById(R.id.tvContactName);
            tvContactPhone = (TextView) itemView.findViewById(R.id.tvContactPhone);
        }

        @Override
        public void onClick(View v) {
            Log.e("itemViewholder", tvContactPhone.getText().toString());
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tvContactPhone.getText().toString()));
            context.startActivity(intent);
        }
    }
}
